package Queries;

public class Query {
	
	private static String url = "jdbc:mysql://localhost/breizhlink";
	private static String utilisateur = "root";
	private static String motDePasse = "";
	
	public static String getUrl() {
		return url;
	}
	public static void setUrl(String url) {
		Query.url = url;
	}
	public static String getUtilisateur() {
		return utilisateur;
	}
	public static void setUtilisateur(String utilisateur) {
		Query.utilisateur = utilisateur;
	}
	public static String getMotDePasse() {
		return motDePasse;
	}
	public static void setMotDePasse(String motDePasse) {
		Query.motDePasse = motDePasse;
	}
	
	
}
