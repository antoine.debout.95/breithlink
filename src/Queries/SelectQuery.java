package Queries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectQuery extends Query {
	
	private static Connection connexion = null;
	private static Statement statement = null;
	
	/* Execute request */
	public static ResultSet execute(String query) {
		try {
			/* DB Connection */
		    connexion = DriverManager.getConnection(Query.getUrl(), Query.getUtilisateur(), Query.getMotDePasse() );
		    statement = connexion.createStatement();
		    
		    return statement.executeQuery(query);
		} catch (SQLException e) {
			/* Return error message */
	    	System.out.print(e.getMessage());
		} finally {
		    if (connexion != null)
		        try {
		            /* Closing connection */
		            connexion.close();
		            statement.close();
		        } catch (SQLException ignore) {
		            /* Ignore errors during enclosure */
		        }
		}
		
		return null;
	}
}
