package Models;

public class Url {

	private int url_id = 0;
	private String url = "";
	private String domain = "";
	private User user = null;
	
	public Url() {
		super();
		this.url_id = 0;
		this.url = null;
		this.domain = null;
		this.user = null;
	}
	
	public Url(int url_id, String url, String domain, User user) {
		super();
		this.url_id = url_id;
		this.url = url;
		this.domain = domain;
		this.user = user;
	}

	public int getUrl_id() {
		return url_id;
	}

	public void setUrl_id(int url_id) {
		this.url_id = url_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Url [url_id=" + url_id + ", url=" + url + ", domain=" + domain + ", user=" + user + "]";
	}
	
}
