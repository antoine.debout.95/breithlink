package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import DAO.ShorturlDAO;

public class Shorturl extends Url {

	private Date startDate = null;
	private Date expireDate = null;
	private int maxClick = 0;
	private boolean active = true;
	private ArrayList<Auth> auths = null;
	private ArrayList<Stat> stats = null;
	
	public Shorturl() {
		super();
		this.startDate = null;
		this.expireDate = null;
		this.maxClick = 0;
		this.active = false;
		this.auths = null;
		this.stats = null;
	}
	
	public Shorturl(int url_id, String url, String domain, User user, Date startDate, Date expireDate, int maxClick,
			boolean active, ArrayList<Auth> auths, ArrayList<Stat> stats) {
		super(url_id, url, domain, user);
		this.startDate = startDate;
		this.expireDate = expireDate;
		this.maxClick = maxClick;
		this.active = active;
		this.auths = auths;
		this.stats = stats;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public int getMaxClick() {
		return maxClick;
	}

	public void setMaxClick(int maxClick) {
		this.maxClick = maxClick;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ArrayList<Auth> getAuths() {
		return auths;
	}

	public void setAuths(ArrayList<Auth> auths) {
		this.auths = auths;
	}

	public ArrayList<Stat> getStats() {
		return stats;
	}

	public void setStats(ArrayList<Stat> stats) {
		this.stats = stats;
	}

	public int save() {
		String query = "INSERT INTO `shorturl`(url, domain, user_id, creationDate)"
				+ "VALUES ('"
				+ this.getUrl() + "', '"
				+ this.getDomain() + "', "
				+ this.getUser().getUser_id() + ", '"
				+ this.getStartDate() + "', '"
				+ this.getExpireDate() + "', "
				+ this.getMaxClick() + ", "
				+ this.isActive() + ")";

		/* Recursive saving on auths */
		for(Auth auth : auths) {
			auth.save();
		}

		/* Recursive saving on stats */
		for(Stat stat : stats) {
			stat.save();
		}
		
		return ShorturlDAO.save(query);
	}
	
	public static Shorturl fetchById(int id) throws SQLException {
		String query = "SELECT * FROM `shorturl` WHERE url_id=" + id;
		ResultSet result = ShorturlDAO.fetch(query);
		Shorturl shorturl = new Shorturl();
		
		// All values
		while(result.next()) {
			shorturl.setUrl(result.getString("url"));
			shorturl.setDomain(result.getString("domain"));
			shorturl.setUser(User.fetchById(result.getInt("user_id")));
			shorturl.setStartDate(result.getDate("startDate"));
			shorturl.setExpireDate(result.getDate("expireDate"));
			shorturl.setMaxClick(result.getInt("maxClick"));
			shorturl.setActive(result.getBoolean("active"));
		}
		
		return shorturl;
	}

	@Override
	public String toString() {
		return "Shorturl [startDate=" + startDate + ", expireDate=" + expireDate + ", maxClick=" + maxClick
				+ ", active=" + active + ", auths=" + auths + ", stats=" + stats + "]";
	}
	
}
