package Models;

import Models.Url;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import DAO.LongurlDAO;

public class Longurl extends Url {

	private Date creationDate = null;
	
	public Longurl() {
		super();
		this.creationDate = null;
	}
	
	public Longurl(int longurl_id, String url, String domain, User user, Date creationDate) {
		super(longurl_id, url, domain, user);
		this.creationDate = creationDate;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int save() {
		String query = "INSERT INTO `longurl`(url, domain, user_id, creationDate)"
				+ "VALUES ('"
				+ this.getUrl() + "', '"
				+ this.getDomain() + "', "
				+ this.getUser().getUser_id() + ", '"
				+ this.getCreationDate() + "')";
		
		return LongurlDAO.save(query);
	}
	
	public static Longurl fetchById(int id) throws SQLException {
		String query = "SELECT * FROM `longurl` WHERE longurl_id=" + id;
		ResultSet result = LongurlDAO.fetch(query);
		Longurl longurl = new Longurl();
		
		while(result.next()) {
			longurl.setUrl(result.getString("url"));
			longurl.setDomain(result.getString("domain"));
			longurl.setUser(User.fetchById(result.getInt("user_id")));
			longurl.setCreationDate(result.getDate("creationDate"));
		}
		
		return longurl;
	}

	@Override
	public String toString() {
		return "Longurl [creationDate=" + creationDate + "]";
	}
	
}
