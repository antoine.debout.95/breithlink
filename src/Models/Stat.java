package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import DAO.StatDAO;

public class Stat {

	private int stat_id = 0;
	private Date dateClick = null;
	private String ipAddress = null;
	private Shorturl shorturl = null;
	
	public Stat() {
		super();
		this.stat_id = 0;
		this.dateClick = null;
		this.ipAddress = null;
		this.shorturl = null;
	}
	
	public Stat(int stat_id, Date dateClick, String ipAddress, Shorturl shorturl) {
		super();
		this.stat_id = stat_id;
		this.dateClick = dateClick;
		this.ipAddress = ipAddress;
		this.shorturl = shorturl;
	}
	
	public int getStat_id() {
		return stat_id;
	}
	public void setStat_id(int stat_id) {
		this.stat_id = stat_id;
	}
	public Date getDateClick() {
		return dateClick;
	}
	public void setDateClick(Date dateClick) {
		this.dateClick = dateClick;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Shorturl getShorturl() {
		return shorturl;
	}
	public void setShorturl(Shorturl shorturl) {
		this.shorturl = shorturl;
	}

	public int save() {
		String query = "INSERT INTO `stat`(dateClick, ipAddress, shorturl_id)"
				+ "VALUES ("
				+ this.getDateClick() + ", '"
				+ this.getIpAddress() + "', "
				+ this.getShorturl().getUrl_id() + ")";
		
		return StatDAO.save(query);
	}
	
	public static Stat fetchById(int id) throws SQLException {
		String query = "SELECT * FROM `stat` WHERE stat_id=" + id;
		ResultSet result = StatDAO.fetch(query);
		Stat stat = new Stat();
		
		while(result.next()) {
			stat.setStat_id(result.getInt("stat_id"));
			stat.setDateClick(result.getDate("dateClick"));
			stat.setIpAddress(result.getString("ipAddress"));
			stat.setShorturl(Shorturl.fetchById(result.getInt("shorturl_id")));
		}
		
		return stat;
	}
	
	@Override
	public String toString() {
		return "Stat [stat_id=" + stat_id + ", dateClick=" + dateClick + ", ipAddress=" + ipAddress + ", shorturl="
				+ shorturl + "]";
	}
	
}
