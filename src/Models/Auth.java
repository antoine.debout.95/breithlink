package Models;

import java.sql.ResultSet;
import java.sql.SQLException;

import DAO.AuthDAO;

public class Auth {
	private int auth_id = 0;
	private boolean email = false;
	private boolean captcha = false;
	private boolean pass = false;
	private String word = null;
	private Shorturl shorturl = null;
	
	public Auth(){
		this.auth_id = 0;
		this.email = false;
		this.captcha = false;
		this.pass = false;
		this.word = null;
		this.shorturl = null;
	}
	
	public Auth(int auth, boolean email, boolean captcha, boolean pass, String word, Shorturl shorturl) {
		this.auth_id = auth;
		this.email = email;
		this.captcha = captcha;
		this.pass = pass;
		this.word = word;
		this.shorturl = shorturl;
	}
	
	public int getAuth_id() {
		return auth_id;
	}
	public void setAuth_id(int auth_id) {
		this.auth_id = auth_id;
	}
	public boolean isEmail() {
		return email;
	}
	public void setEmail(boolean email) {
		this.email = email;
	}
	public boolean isCaptcha() {
		return captcha;
	}
	public void setCaptcha(boolean captcha) {
		this.captcha = captcha;
	}
	public boolean isPass() {
		return pass;
	}
	public void setPass(boolean pass) {
		this.pass = pass;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public Shorturl getShorturl() {
		return shorturl;
	}
	public void setShorturl(Shorturl shorturl) {
		this.shorturl = shorturl;
	}
	
	public int save() {
		String query = "INSERT INTO `auth`(email, captcha, pass, word, shorturl_id)"
				+ "VALUES ("
				+ this.isEmail() + ", "
				+ this.isCaptcha() + ", "
				+ this.isPass() + ", '"
				+ this.getWord() + "', "
				+ this.getShorturl().getUrl_id() + ")";

		return AuthDAO.save(query);
	}
	
	public static Auth fetchById(int id) throws SQLException {
		String query = "SELECT * FROM `auth` WHERE auth_id=" + id;
		ResultSet result = AuthDAO.fetch(query);
		Auth auth = new Auth();
		
		while(result.next()) {
			auth.setAuth_id(result.getInt("auth_id"));
			auth.setEmail(result.getBoolean("email"));
			auth.setCaptcha(result.getBoolean("captcha"));
			auth.setPass(result.getBoolean("pass"));
			auth.setWord(result.getString("word"));
			auth.setShorturl(Shorturl.fetchById(result.getInt("shorturl_id")));
		}
		
		return auth;
	}

	@Override
	public String toString() {
		return "Auth [auth_id=" + auth_id + ", email=" + email + ", captcha=" + captcha + ", pass=" + pass + ", word="
				+ word + ", shorturl=" + shorturl + "]";
	}
	
	
}
