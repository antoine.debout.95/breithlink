package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DAO.UserDAO;

public class User {

	private int user_id = 0;
	private String firstname = "";
	private String lastname = "";
	private String email = "";
	private String login = "";
	private String password = "";
	private Type type = null;
	private boolean active = true;
	private ArrayList<Longurl> longurls = null;
	private ArrayList<Shorturl> shorturls = null;
	
	public User() {
		super();
		this.user_id = 0;
		this.firstname = null;
		this.lastname = null;
		this.email = null;
		this.login = null;
		this.password = null;
		this.type = null;
		this.active = false;
		this.longurls = null;
		this.shorturls = null;
	}
	
	public User(int user_id, String firstname, String lastname, String email, String login, String password,
			Type type, boolean active, ArrayList<Longurl> longurls, ArrayList<Shorturl> shorturls) {
		super();
		this.user_id = user_id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.login = login;
		this.password = password;
		this.type = type;
		this.active = active;
		this.longurls = longurls;
		this.shorturls = shorturls;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public ArrayList<Longurl> getLongurls() {
		return longurls;
	}

	public void setLongurls(ArrayList<Longurl> longurls) {
		this.longurls = longurls;
	}

	public ArrayList<Shorturl> getShorturls() {
		return shorturls;
	}

	public void setShorturls(ArrayList<Shorturl> shorturls) {
		this.shorturls = shorturls;
	}

	public int save() {
		String query = "INSERT INTO `user`(firstname, lastname, email, login, password, type, active)"
				+ "VALUES ('"
				+ this.getFirstname()+ "', '"
				+ this.getLastname()+ "', '"
				+ this.getEmail()+ "', '"
				+ this.getLogin()+ "', '"
				+ this.getPassword()+ "', '"
				+ this.getType()+ "', "
				+ this.isActive() + ")";
		
		/* Recursive saving on long urls */
		for(Longurl lurl : longurls) {
			lurl.save();
		}
		
		/* Recursive saving on short urls */
		for(Shorturl surl : shorturls) {
			surl.save();
		}
		
		return UserDAO.save(query);
	}
	
	public static User fetchById(int id) throws SQLException {
		String query = "SELECT * FROM `user` WHERE user_id=" + id;
		ResultSet result = UserDAO.fetch(query);
		User user = new User();
		
		while(result.next()) {
			user.setUser_id(result.getInt("user_id"));
			user.setFirstname(result.getString("firstname"));
			user.setLastname(result.getString("lastname"));
			user.setEmail(result.getString("email"));
			user.setLogin(result.getString("login"));
			user.setPassword(result.getString("password"));
			user.setActive(result.getBoolean("active"));
		}
		
		return user;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", login=" + login + ", password=" + password + ", type=" + type + ", active=" + active + "]";
	}
	
}
