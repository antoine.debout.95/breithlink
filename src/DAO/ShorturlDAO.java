package DAO;

import java.sql.ResultSet;

import Queries.*;

public class ShorturlDAO {

	public static int save(String query) {
		return InsertQuery.execute(query);
	}

	public static ResultSet fetch(String query) {
		return SelectQuery.execute(query);
	}
}
