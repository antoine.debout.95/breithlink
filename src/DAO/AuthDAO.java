package DAO;

import java.sql.ResultSet;

import Queries.*;

public class AuthDAO {

	public static int save(String query) {
		return InsertQuery.execute(query);
	}
	
	public static ResultSet fetch(String query) {
		return SelectQuery.execute(query);
	}
}
