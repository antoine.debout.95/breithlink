<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="../CSS/style.css">
		<title>Raccourcir</title>
	</head>
<body>

<%@ include file="../HTML/Head.html" %>
<%@ include file="Header.jsp" %>

<div>
	<label for="firstname">Prénom</label>
	<input type="text" name="firstname" value="<%= session.getAttribute("firstname") %>" readonly="readonly" />
</div>

<div>
	<label for="lastname">Nom</label>
	<input type="text" name="lastname" value="<%= session.getAttribute("lastname") %>" readonly="readonly" />
</div>

<div>
	<label for="email">Mail</label>
	<input type="email" name="email" value="<%= session.getAttribute("email") %>" readonly="readonly" />
</div>

<div>
	<label for="type">Type</label>
	<input type="text" name="type" value="<%= session.getAttribute("type") %>" readonly="readonly" />
</div>

<%
if(session.getAttribute("actif") != null && session.getAttribute("actif").equals(true)) {
	%>
<div>
	<label for="Actif">Actif</label>
	<input type="radio" name="actif" value="Actif" checked="checked" readonly="readonly" />
	<label for="Inactif">Inactif</label>
	<input type="radio" name="actif" value="Inactif" readonly="readonly" />
</div>
	<%
} else {
	%>
<div>
	<label for="Actif">Actif</label>
	<input type="radio" name="actif" value="Actif" readonly="readonly" />
	<label for="Inactif">Inactif</label>
	<input type="radio" name="actif" value="Inactif" checked="checked" readonly="readonly" />
</div>
	<%
} %>

<%@ include file="../HTML/Footer.html" %>

</body>
</html>