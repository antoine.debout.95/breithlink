<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="../CSS/style.css">
		<title>Raccourcir</title>
	</head>
<body>

<%@ include file="../HTML/Head.html" %>
<%@ include file="Header.jsp" %>

<fieldset>

	<%
	if(session.getAttribute("Logged") == null) {
		%>
	<legend>Connexion</legend>
	
	<form action="/Connect" method="POST">
		<div>
			<label for="Login">Login</label>
			<input type="text" name="Login" placeholder="Login" />
		</div>
		
		<div>
			<label for="Pass">Mot de passe</label>
			<input type="password" name="Pass" placeholder="****" />
		</div>
		
		<input type="submit" value="Connexion" />
		
	</form>
		<%
	} else {
		%>
	<legend>Déconnexion</legend>
	
	<form action="/Connect" method="POST">
		<input type="submit" value="Déconnexion" />
	</form>
		<%
	}
	%>
</fieldset>

<%@ include file="../HTML/Footer.html" %>

</body>
</html>