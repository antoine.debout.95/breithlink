<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="../CSS/style.css">
		<title>Raccourcir</title>
	</head>
<body>

<%@ include file="../HTML/Head.html" %>
<%@ include file="Header.jsp" %>

<div>
	<ul>
		<li><a href="Data.jsp">Informations personnelles</a></li>
		<li><a href="URLs.jsp">Mes URLs</a></li>
	</ul>
</div>

<%@ include file="../HTML/Footer.html" %>

</body>
</html>