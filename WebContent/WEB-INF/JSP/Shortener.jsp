<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="../CSS/style.css">
		<title>Raccourcir</title>
	</head>
<body>

<%@ include file="../HTML/Head.html" %>
<%@ include file="Header.jsp" %>

<form action="/Shorten" method="POST">
	<div>
		<label for="url">URL � raccourcir</label>
		<input type="text" name="url" placeholder="https://www.youtube.com/watch?v=5hS9rnn5-_c" />
		<input type="submit" value="Raccourcir" />
	</div>
	<%
	if(session.getAttribute("Logged") != null) {
		%>
	<div>
		<input type="checkbox" name="Captcha" />
		<label for="Captcha">Captcha</label>
	</div>
		<%
	}
	%>
	
	<div>
		<input type="checkbox" name="Pass" onclick="change()"/>
		<label for="Pass">S�curis�e avec mot de passe</label>
	</div>
	
	<div>
		<input type="password" name="Word" />
	</div>
	
	<%
	if(session.getAttribute("Logged") != null) {
		%>
	<div>
		<input type="checkbox" name="Range" />
		<label for="From">Valable du</label>
		<input type="date" name="From" />
		<label for="To">au</label>
		<input type="date" name="To" />
	</div>
	
	<div>
		<input type="checkbox" name="Until" />
		<label for="UntilT">Valable jusqu'au</label>
		<input type="date" name="UntilT" placeholder="11/11/2111" />
	</div>
	
	<div>
		<input type="checkbox" name="Clics" />
		<label for="Clics">Max</label>
		<input type="number" name="Clics" />
		<label for="Clics">clics</label>
	</div>
		<%
	}
	
	if(session.getAttribute("Logged") == null) {
		%>
	<p>Afin de voir l'ensemble des options disponible, veuillez <a href="../Accout">cr�er un compte</a> ou vous <a href="../Connect">connecter</a></p>
		<%
	}
	%>
</form>

<script src="../JS/switch.js"></script>

<%@ include file="../HTML/Footer.html" %>

</body>
</html>