<div class="header">
	<ul>
		<li><a href="Shortener.jsp">Acceuil</a></li>
		<li><a href="Presentation.jsp">Présentation</a></li>
		<%
		String connexion = "";
		
		if(session.getAttribute("Logged") == null) {
			%>
			<li><a href="Account.jsp">Création d'un compte</a></li>
			<%
			connexion = "Connexion";
		} else {
			%>
			<li><a href="Shortener.jsp">Raccourcir</a></li>
			<li><a href="AccountMenu.jsp">Mon Compte</a></li>
			<%

			connexion = "Déconnexion";
		}
		%>
		
		<li><a href="Connect.jsp"><%= connexion %></a></li>
	</ul>
</div>