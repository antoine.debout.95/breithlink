<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="../CSS/style.css">
		<title>Raccourcir</title>
	</head>
<body>

<%@ include file="../HTML/Head.html" %>
<%@ include file="Header.jsp" %>

<div>
	<p>Bonjour <%= session.getAttribute("firstname") %>,</p>
	<p>vous pouvez désormais accéder é toutes nos options de création</p>
	<p>d'URL raccourcies</p>
	<p>Avec mot de passe</p>
	<p>Avec mot de passe différents</p>
	<p>A durée limitée</p>
	<p>A durée périodique</p>
	<p>Création par lots</p>
	<p>Visualisation des statistiques</p>
</div>

<%@ include file="../HTML/Footer.html" %>

</body>
</html>